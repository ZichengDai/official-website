import $ from 'jquery';
import Head from 'next/head';
import Link from 'next/link';
import styles from './index.module.scss';
import Menu, { Item as MenuItem } from 'rc-menu';
import Dropdown from 'rc-dropdown';
import 'rc-dropdown/assets/index.css';
import { Controller, Scene } from 'react-scrollmagic';
import { Tween, Timeline } from 'react-gsap';

interface navType {
	navName: string;
	children?: string[];
}

interface footerType {
	title: string;
	list: string[];
	last?: boolean;
}
export default function Home() {
	const navData: navType[] = [
		{
			navName: '首页',
		},
		{
			navName: '产品',
		},
		{
			navName: '解决方案',
			children: ['自动化与DevsecOps', '安全合规性验证', '智能容器安全', '云主机安全'],
		},
		{
			navName: '公司',
			children: ['关于探针', '联系我们', '资讯'],
		},
	];

	const footerData: footerType[] = [
		{
			title: '产品',
			list: ['领航-容器安全'],
		},
		{
			title: '解决方案',
			list: ['自动化与DevsecOps', '安全合规性验证', '智能容器安全', '云主机安全'],
		},
		{
			title: '公司',
			list: ['关于探真', '联系我们', '加入探真', '资讯'],
		},
		{
			title: '友情链接',
			list: ['Lorem ipsum', 'dolor sit amet,', 'consectetur'],
		},
		{
			title: '在线咨询',
			list: ['info@tensorsecurity.cn', '400-000-0000'],
			last: true,
		},
	];

	let scLogo: string = '/image/scLogo.png';
	let Logo: string = '/image/head_logo.png';

	let createMenuItems = (data: string[]) => {
		let menuItems = [];
		data.map((item, ind) => {
			menuItems.push(<MenuItem key={ind}>{item}</MenuItem>);
		});
		return menuItems;
	};

	let createMenu = (data: string[]) => {
		return <Menu>{createMenuItems(data)}</Menu>;
	};

	if (process.browser) {
		let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;

		let headerWrap = $('.header');
		let headerLogo = $('.header-logo');

		let scrollHeader = () => {
			headerWrap.addClass('header-bg');
			headerLogo.attr('src', scLogo);
		};

		let topHeader = () => {
			headerWrap.removeClass('header-bg');
			headerLogo.attr('src', Logo);
		};

		// 页面刷新时，如果滚动条不在顶部，导航需加背景色
		window.onload = function () {
			if (scrollTop > 0) {
				scrollHeader();
			}
		};

		// 页面滚动时页头背景色变化
		window.onscroll = function () {
			scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
			if (scrollTop > 0) {
				scrollHeader();
			} else {
				topHeader();
			}
		};
	}

	return (
		<div className="container">
			<Head>
				<title>tz</title>
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<div className={styles.banner}>
				<header className={`${styles.nav} header`}>
					<div className={styles.nav_con}>
						<a href="" className={`${styles.nav_con_logo}`} id="test">
							<img src={Logo} alt="" className="header-logo" />
						</a>
						<ul className={styles.nav_con_font}>
							{navData.map((res, index) => {
								return res.children ? (
									<Dropdown overlay={createMenu(res.children)} overlayClassName={styles.navDown} key={index} placement="bottomCenter" minOverlayWidthMatchTrigger={false} animation="slide-up">
										<li className="test">{res.navName}</li>
									</Dropdown>
								) : (
									<li key={index}>{res.navName}</li>
								);
							})}
						</ul>
						<div className={styles.nav_con_btn}>申请免费试用</div>
					</div>
				</header>
				<video muted loop src="/video/bg-video.mp4" autoPlay></video>
				<div className={styles.font}>
					<div className={styles.font_box}>
						<h4>探真科技——探索求真数字安全世界</h4>
						<h2>领航-容器安全 </h2>
						<h2>面向容器全生命周期的安全解决方案</h2>
						<div className={styles.btn}>
							<a href="" className={styles.btn_free}>
								申请免费试用
							</a>
						</div>
					</div>
				</div>
			</div>

			<div className={styles.scrollBox}>
				<Controller>
					<Scene duration={10} pin="#scrollPin">
						<div className={`${styles.scrollBox_content}`} id="scrollPin">
							<div className={styles.scrollBox_content_wrap}>
								<div className={styles.scrollBox_content_header}>
									<h3>
										领航容器安全-核心应用场景
										<h4></h4>
									</h3>
								</div>
								<div className={styles.scrollBox_content_main}>
									<div className={styles.scrollBox_content_main_left}>
										<div className={styles.scrollBox_content_main_left_title}>合规管理</div>
										<div className={styles.scrollBox_content_main_left_line}></div>
										<p>
											在容器化应用平台的整个生命周期中。领航集成多种漏洞威胁情报源，基于内置的漏洞管理安全策略，提供对容器镜像和镜像仓库的漏洞、病毒木马、恶意插件、以及任何嵌入的机密进行发现并给出相应修复建议
										</p>
									</div>
									<div className={styles.scrollBox_content_main_right}>
										<img src="/image/management.png" alt="" />
									</div>
								</div>
							</div>
						</div>
					</Scene>

	
					{/* <Scene  >
						<div className={`${styles.scrollBox_content}`}>
							<div className={styles.scrollBox_content_wrap}>
								<div className={styles.scrollBox_content_header}>
									<h3>
										领航容器安全-核心应用场景
										<h4></h4>
									</h3>
								</div>
								<div className={styles.scrollBox_content_main}>
									<div className={styles.scrollBox_content_main_left}>
										<div className={styles.scrollBox_content_main_left_title}>合规管理</div>
										<div className={styles.scrollBox_content_main_left_line}></div>
										<p>
											在容器化应用平台的整个生命周期中。领航集成多种漏洞威胁情报源，基于内置的漏洞管理安全策略，提供对容器镜像和镜像仓库的漏洞、病毒木马、恶意插件、以及任何嵌入的机密进行发现并给出相应修复建议
										</p>
									</div>
									<div className={styles.scrollBox_content_main_right}>
										<img src="/image/management.png" alt="" />
									</div>
								</div>
							</div>
						</div>
					</Scene>

					<Scene  >
						<div className={`${styles.scrollBox_content} ${styles.scrollBox_noBg}`}>
							<div className={styles.scrollBox_content_wrap}>
								<div className={styles.scrollBox_content_header}>
									<h3>
										领航容器安全-核心应用场景
										<h4></h4>
									</h3>
								</div>
								<div className={styles.scrollBox_content_main}>
									<div className={styles.scrollBox_content_main_left}>
										<div className={styles.scrollBox_content_main_left_title}>合规管理</div>
										<div className={styles.scrollBox_content_main_left_line}></div>
										<p>
											在容器化应用平台的整个生命周期中。领航集成多种漏洞威胁情报源，基于内置的漏洞管理安全策略，提供对容器镜像和镜像仓库的漏洞、病毒木马、恶意插件、以及任何嵌入的机密进行发现并给出相应修复建议
										</p>
									</div>
									<div className={styles.scrollBox_content_main_right}>
										<img src="/image/management.png" alt="" />
									</div>
								</div>
							</div>
						</div>
					</Scene>

					<Scene >
						<div className={`${styles.scrollBox_content} ${styles.scrollBox_noBg}`}>
							<div className={styles.scrollBox_content_wrap}>
								<div className={styles.scrollBox_content_header}>
									<h3>
										领航容器安全-核心应用场景
										<h4></h4>
									</h3>
								</div>
								<div className={styles.scrollBox_content_main}>
									<div className={styles.scrollBox_content_main_left}>
										<div className={styles.scrollBox_content_main_left_title}>合规管理</div>
										<div className={styles.scrollBox_content_main_left_line}></div>
										<p>
											在容器化应用平台的整个生命周期中。领航集成多种漏洞威胁情报源，基于内置的漏洞管理安全策略，提供对容器镜像和镜像仓库的漏洞、病毒木马、恶意插件、以及任何嵌入的机密进行发现并给出相应修复建议
										</p>
									</div>
									<div className={styles.scrollBox_content_main_right}>
										<img src="/image/management.png" alt="" />
									</div>
								</div>
							</div>
						</div>
					</Scene>

					<Scene >
						<div className={`${styles.scrollBox_content} ${styles.scrollBox_noBg}`}>
							<div className={styles.scrollBox_content_wrap}>
								<div className={styles.scrollBox_content_header}>
									<h3>
										领航容器安全-核心应用场景
										<h4></h4>
									</h3>
								</div>
								<div className={styles.scrollBox_content_main}>
									<div className={styles.scrollBox_content_main_left}>
										<div className={styles.scrollBox_content_main_left_title}>合规管理</div>
										<div className={styles.scrollBox_content_main_left_line}></div>
										<p>
											在容器化应用平台的整个生命周期中。领航集成多种漏洞威胁情报源，基于内置的漏洞管理安全策略，提供对容器镜像和镜像仓库的漏洞、病毒木马、恶意插件、以及任何嵌入的机密进行发现并给出相应修复建议
										</p>
									</div>
									<div className={styles.scrollBox_content_main_right}>
										<img src="/image/management.png" alt="" />
									</div>
								</div>
							</div>
						</div>
					</Scene> */}
				</Controller>
			</div>

			<section className={styles.footer}>
				<div className={styles.footer_wrap}>
					<div className={styles.footer_wrap_logo}>
						<img src="/image/footerLogo.png" alt="" />
					</div>
					<div className={styles.footer_wrap_box}>
						<div className={styles.footer_wrap_left}>
							<p>
								探真科技 TensorSecurity
								Technology由硅谷网络安全团队为核心归国创建，专注于为中国企业提供便捷、可视、智能的云原生安全解决方案。围绕云原生、微服务、主机、容器、K8s、数据等各各层面相应提供漏洞、合规、威胁入侵检测、企业数据隐私防护等安全保障的同时，自动化与整个应用程序迭代生命周期相融合，持续提升企业在云原生业务环境下的安全运维管理能力，实现AI赋能的智能安全运维，DevSecOps.
							</p>
						</div>
						<div className={styles.footer_wrap_right}>
							{footerData.map((res, ind) => {
								return (
									<ul>
										<div key={ind} className={styles.footer_wrap_right_title}>
											{res.title}
										</div>
										{res.list.map((item, index) => {
											return <li key={index}>{item}</li>;
										})}
										{res.last ? (
											<div className={styles.footer_wrap_right_concern}>
												<h2>关注我们</h2>
												<div className={styles.footer_wrap_right_concern_img}></div>
											</div>
										) : (
											''
										)}
									</ul>
								);
							})}
						</div>
					</div>

					<div className={styles.footer_wrap_bottomLine}></div>
					<div className={styles.footer_wrap_copyright}>北京探真科技有限公司©2007-2020版权所有 京ICP备08104828号-4 京公网安备11010502033785号</div>
				</div>
			</section>
			<style jsx>{`
				.header-bg {
					background: #ffffff;
					box-shadow: 0px 10px 40px 0px rgba(0, 0, 0, 0.07);
				}
				.header-bg ul li {
					color: #000;
				}
			`}</style>

			<style jsx global>{`
				.rc-dropdown-menu > .rc-dropdown-menu-item:hover,
				.rc-dropdown-menu > .rc-dropdown-menu-item-active,
				.rc-dropdown-menu > .rc-dropdown-menu-item-selected {
					background: rgba(179, 208, 255, 0.3) !important;
				}
				.rc-dropdown-menu > .rc-dropdown-menu-item-selected:after {
					display: none;
				}
			`}</style>
		</div>
	);
}
