import Link from 'next/link';

export default function NoFound({}) {
	return (
		<div className="container">
			<Link href="/">NoFound</Link>
		</div>
	);
}
