const webpack = require('webpack')
module.exports = {
  webpack: (config, {
    dev
  }) => {
    config.plugins.push(
      new webpack.ProvidePlugin({
        '$': 'jquery',
        'jQuery': 'jquery',
      })
    )
    return config
  },
  exportPathMap: function () {
    return {
      '/': {
        page: '/'
      },
      '/posts/first-post': {
        page: '/posts/first-post'
      },
    }
  },
}