module.exports = {
  plugins: {
    'postcss-preset-env': {
      autoprefixer: {
        flexbox: 'no-2009'
      },
      stage: 3,
      features: {
        'custom-properties': false,
        "nesting-rules": true // Enable nesting
      }
    },
    // 'postcss-px-to-viewport': {
    //   viewportWidth: 1920, // 设计稿宽度
    //   // viewportHeight: 1080, // (设计稿高度，可以不指定)
    //   unitPrecision: 3, // px to vw无法整除时，保留几位小数
    //   viewportUnit: 'vw', // 转换成vw单位
    //   selectorBlackList: ['.ignore', '.hairlines'], // (不转换的类名)
    //   minPixelValue: 1, // (Number) 小于1px不转换
    //   mediaQuery: false, // (Boolean) 允许媒体查询中转换.
    // }
  }
}